import React, { useEffect, useState } from "react";
// import logo from './logo.svg';
import './App.css';
import Layout from './Layout';
import ListProduct from "./components/ListProduct";
import AddSeller from "./components/AddSeller";
import AddProduct from "./components/AddProduct";
import SearchProduct from "./components/SearchProduct";
import Hello from "./components/Hello";

function App() {
  const [currentMenu, setCurrentMenu] = useState('hello')

  return (
    <Layout>
      <table className="menu" cellPadding={0} cellSpacing={0} width="100%">
        <thead>
          <tr>
            <td onClick={()=>setCurrentMenu('hello')}>Home</td>
            <td onClick={()=>setCurrentMenu('addSeller')}>Tambah Penjual</td>
            <td onClick={()=>setCurrentMenu('AddProduct')}>Tambah Produk</td>
            <td onClick={()=>setCurrentMenu('ListProduct')}>List Produk</td>
            <td onClick={()=>setCurrentMenu('SearchProduct')}>Cari Produk</td>
          </tr>
        </thead>
      </table>
      <div className="content">
        {currentMenu == 'hello' && <Hello/>}
        {currentMenu == 'addSeller' && <AddSeller/>}
        {currentMenu == 'AddProduct' && <AddProduct/>}
        {currentMenu == 'ListProduct' && <ListProduct/>}
        {currentMenu == 'SearchProduct' && <SearchProduct/>}
      </div>
    </Layout>
  );
}

export default App;
