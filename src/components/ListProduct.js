import React,{useState, useEffect} from "react";

const endpoint = "/listProductBySellerId?seller_id="

function ListProduct(){
    const [products,setProducts] = useState([])
    const [sellerId, setSellerId] = useState("")
    const [isLoading,setLoading] = useState(false)
    const [isError,setError] =  useState(false)
    const handleChange = (event) =>{
        if(event.key === "Enter"){
            setSellerId(event.target.value)
        }
    }
    useEffect(()=>{
        setProducts([])
        if(sellerId !== ""){
            const fetchData = async () => {
                setLoading(true)
                const response = await fetch(`${endpoint}${sellerId}`)
                const result = await response.json()
                console.log(result)
                setProducts(result.data)
                if(result.code !== 200){
                    alert(result.message)
                    setError(true)
                }
                setLoading(false)
            } 
            fetchData()
        }
    },[sellerId])

    return(
        <>
        <h3>List Product by Seller Id </h3>
        Seller ID : <input type="text" name="sellerId" placeholder="Press enter to search" onKeyDown={handleChange}></input>
        <br/><br/>
        {isLoading && <p>Loading....</p>}
        {!isError ?
        <table width="100%" cellPadding={0} cellSpacing={0} className="table">
            <thead>
            <tr>
                <th>ID</th>
                <th>Nama</th>
                <th>Satuan</th>
                <th>Harga Satuan</th>
                <th>Deskripsi</th>
            </tr>
            </thead>
            <tbody>
            {products.map((item,index) => (
                <tr key={item.id}>
                    <td>{item.id}</td>
                    <td>{item.nama}</td>
                    <td>{item.satuan}</td>
                    <td>{item.hargaSatuan}</td>
                    <td>{item.deskripsi}</td>
                </tr>
            ))}
            </tbody>
        </table>
        : <p>Loading Data Error</p> }
        </>
    )
}

export default ListProduct;