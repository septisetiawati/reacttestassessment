import React,{useState, useEffect} from "react";

const defaultProduct = {
    code : 0,
    status : "",
    message : "",
    data : []
}
const endpoint = "/searchProductByKeyword?keyword="

function SearchProduct(){
    const [products,setProducts] = useState(defaultProduct)
    const [keyword, setKeyword] = useState("")
    const [isLoading,setLoading] = useState(false)
    const [isError,setError] =  useState(false)
    const onChangeField = (e) => {
        if(e.key === "Enter"){
            setKeyword(e.target.value)
        }
    }
    useEffect(()=>{
        const fetchData = async () => {
            setLoading(true)
            try {
                const response = await fetch(`${endpoint}${keyword}`)
                const result = await response.json()
                setProducts(current=>{
                    return(
                        {
                            ...result,
                            products : [...result.data]
                        }
                    )
                })   
                if(result.status == "Error"){
                    throw new Error('Error')
                }
            } catch (error) {
                setError(true)
            }
            setLoading(false)
        } 
        fetchData()
    },[keyword])

    return(
        <>
        <h3>Search Product</h3>
        Keyword : <input type="text" name="search" placeholder="Press enter to search" onKeyDown={onChangeField}/>
        <br/><br/>
        {isLoading && <p>Loading....</p>}
        {isError && <p>Loading Data Error</p>}
        <table cellPadding={0} cellSpacing={0} className="table" width="100%">
            <thead>
            <tr>
                <th>ID</th>
                <th>Nama</th>
                <th>Satuan</th>
                <th>Harga Satuan</th>
                <th>Seller ID</th>
                <th>Deskripsi</th>
            </tr>
            </thead>
            <tbody>
            {products.data.map((item,index) => (
                <tr key={item.id}>
                    <td>{item.id}</td>
                    <td>{item.nama}</td>
                    <td>{item.satuan}</td>
                    <td>{item.hargaSatuan}</td>
                    <td>{item.sellerId}</td>
                    <td>{item.deskripsi}</td>
                </tr>
            ))}
            </tbody>
        </table>
        </>
    )
}

export default SearchProduct;