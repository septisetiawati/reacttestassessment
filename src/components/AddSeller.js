import React, {useState} from "react";
import useForm from "./CustomHooks";


const endpoint = "/addSeller"

function AddSeller(){

    const [newSeller, setNewSeller] = useState()
    const [isLoading,setLoading] = useState(false)
    const [isError,setError] =  useState(false)
    const saveData = async () => {
        setLoading(true)
        try {
            const response = await fetch(`${endpoint}`,{
                method: "POST",
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify(inputs)
            })
            const result = await response.json();
            if(result.status == "Success"){
                alert(`Berhasil menambahkan Penjual, ID : ${result.data.id}`)
            } else {
                alert(result.message)
            }
        } catch (error) {
            setError(true)
        }
        setLoading(false)
    } 
    const {inputs,handleInputChange,handleSubmit} = useForm(saveData);

    return(
        <form onSubmit={handleSubmit}>
        <h3>Tambah Penjual</h3>
            <table>
                <tbody>
                    <tr>
                        <td>Nama </td>
                        <td>: <input type="text" name="nama" required onChange={handleInputChange} /></td>
                    </tr>
                    <tr>
                        <td>Kota </td>
                        <td>: <input type="text" name="kota" required onChange={handleInputChange}/></td>
                    </tr>
                    <tr>
                        <td colSpan={2} align="right"><input type="submit" value="Simpan"/></td>
                    </tr>
                </tbody>
            </table>
        </form>
    )
}
export default AddSeller;