import React, {useState} from "react";
import useForm from "./CustomHooks";


const endpoint = "/addProduct"

function AddProduct(){

    // const [formInput,setFormInputs] = useState({})
    const [isLoading,setLoading] = useState(false)
    const [isError,setError] =  useState(false)
    const saveData = async () => {
        setLoading(true)
        try {
            const response = await fetch(`${endpoint}`,{
                method: "POST",
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify(inputs)
            })
            const result = await response.json()
            if(result.status == "Success"){
                alert("Berhasil menambahkan Produk")
            } else {
                alert(result.message)
            }
        } catch (error) {
            setError(true)
        }
        setLoading(false)
    } 
    const {inputs,handleInputChange,handleSubmit} = useForm(saveData);

    return(
        <form onSubmit={handleSubmit}>
        {/* <input type="hidden" name="sellerId" onChange={handleInputChange} value="141"/> */}
        <h3>Tambah Produk</h3>
            <table>
                <tbody>
                    <tr>
                        <td>Seller Id </td>
                        <td>: <input type="text" name="sellerId" required onChange={handleInputChange} /></td>
                    </tr>
                    <tr>
                        <td>Nama </td>
                        <td>: <input type="text" name="nama" required onChange={handleInputChange} /></td>
                    </tr>
                    <tr>
                        <td>Satuan </td>
                        <td>: <input type="text" name="satuan" required onChange={handleInputChange}/></td>
                    </tr>
                    <tr>
                        <td>Harga Satuan </td>
                        <td>: <input type="text" name="hargaSatuan" required onChange={handleInputChange}/></td>
                    </tr>
                    <tr>
                        <td>Deskripsi </td>
                        <td>: <textarea name="deskripsi" required onChange={handleInputChange}></textarea></td>
                    </tr>
                    <tr>
                        <td colSpan={2} align="right"><input type="submit" value="Simpan"/></td>
                    </tr>
                </tbody>
            </table>
        </form>
    )
}
export default AddProduct;