import React from 'react';
import logo from './logo.svg';
// import './App.css';

function Layout(props) {
  return (
    <div className="App">
      <header className="App-header">
        <h1>ALAMI ASSESSMENT TEST</h1>
      </header>
      <main className="App-body">{props.children}</main>
      <footer className="App-footer">
        
      </footer>
    </div>
  );
}

export default Layout;
